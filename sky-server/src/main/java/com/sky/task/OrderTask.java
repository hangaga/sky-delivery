package com.sky.task;

import com.sky.constant.MessageConstant;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
public class OrderTask {

    @Autowired
    private OrdersMapper ordersMapper;

    /*
        定时取消 未支付超时的订单
     */
    @Scheduled(cron = "0/30 * * * * ?")//每30秒执行一次
    public void cancleOrder(){
        LocalDateTime before15MinTime = LocalDateTime.now().plusMinutes(-15);
        List<Orders> ordersList = ordersMapper.selectByStatusAndLtTime(Orders.ORDER_STAUTS_PENDING_PAYMENT,before15MinTime);
        if(!CollectionUtils.isEmpty(ordersList)){
            ordersList.stream().forEach(orders -> {
                log.info("订单超时自动取消：{}",orders.getId());
                orders.setStatus(Orders.ORDER_STAUTS_CANCELLED);
                orders.setCancelTime(LocalDateTime.now());
                orders.setCancelReason(MessageConstant.ORDER_TIMEOUT);
                ordersMapper.updateStatus(orders);
            });
        }
    }

    /*
        定时完成 派送超时的订单
     */
    //@Scheduled(cron = "0 0 1 * * ?")//每日凌晨一点执行
    @Scheduled(cron = "0/10 * * * * ?")
    public void completeOrder(){
        LocalDateTime before2HourTime = LocalDateTime.now().minusHours(2);
        List<Orders> ordersList = ordersMapper.selectByStatusAndLtTime(Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS,before2HourTime);
        if(!CollectionUtils.isEmpty(ordersList)){
            ordersList.stream().forEach(orders -> {
                log.info("订单超时自动完成：{}",orders.getId());
                orders.setStatus(Orders.ORDER_STAUTS_COMPLETED);
                orders.setDeliveryTime(LocalDateTime.now());
                ordersMapper.updateStatus(orders);
            });
        }


    }

}
