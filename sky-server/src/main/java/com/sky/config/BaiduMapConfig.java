package com.sky.config;

import com.sky.utils.BaiduMapUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class BaiduMapConfig {

    @Bean
    public BaiduMapUtils deliveryUtils(){
        log.info("项目启动加载地图配置类");
        return new BaiduMapUtils();
    }
}
