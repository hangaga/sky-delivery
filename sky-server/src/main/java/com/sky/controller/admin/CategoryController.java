package com.sky.controller.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController("adminCateGory")
@RequestMapping("/admin/category")
@Api(tags = "分类管理")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 动态查询分类信息
     *
     * @param categoryPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "动态查询分类信息")
    public Result page(CategoryPageQueryDTO categoryPageQueryDTO) {
        log.info("分类分页查询:{}", categoryPageQueryDTO);
        PageResult pageResult = categoryService.list(categoryPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 根据id查询分类信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "根据id查询分类信息")
    public Result findById(@PathVariable Long id) {
        log.info("根据id查询分类信息：id={}", id);
        Category category = categoryService.findById(id);
        return Result.success(category);
    }

    /**
     * 修改分类
     *
     * @param categoryDTO
     * @return
     */
    @PutMapping
    @ApiOperation(value = "修改分类")
    public Result update(@RequestBody CategoryDTO categoryDTO) {
        log.info("修改分类信息：{}", categoryDTO);
        categoryService.update(categoryDTO);
        return Result.success();
    }

    /**
     * 启用、禁用分类
     * @param status
     * @param id
     * @return
     */
    @PutMapping("/status/{status}/{id}")
    @ApiOperation(value = "启用、禁用分类数据信息")
    public Result enableOrDisenable(@PathVariable Integer status, @PathVariable Long id) {
        log.info("启用/禁用分类：status={},id={}", status, id);
        categoryService.enableOrDisenable(status, id);
        return Result.success();
    }

    /**
     * 新增分类信息
     * @param categoryDTO
     * @return
     */
    @PostMapping
    @ApiOperation(value = "新增分类信息")
    public Result insert(@RequestBody CategoryDTO categoryDTO){
        log.info("新增分类信息:{}",categoryDTO);
        categoryService.insert(categoryDTO);
        return Result.success();
    }

    /**
     * 根据id删除分类
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "根据id删除分类")
    public Result deleteById(@PathVariable Long id){
        log.info("根据id删除分类：{}",id);
        categoryService.deleteById(id);
        return Result.success();
    }

    /**
     * 根据类型查询分类信息
     * @param type
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "根据类型查询分类信息")
    public Result findByType(Integer type){
        log.info("根据类型查询分类信息：{}",type);
        List<Category> list = categoryService.findByType(type);
        return Result.success(list);
    }


}
