package com.sky.controller.admin;

import com.sky.constant.StatusConstant;
import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController("adminShopController")
@RequestMapping("/admin/shop")
@Api(tags = "店铺操作接口")
public class ShopStatusController {

    private static final String SHOP_STATUS = "shop:status";

    @Autowired
    RedisTemplate redisTemplate;

    @GetMapping("/status")
    @ApiOperation("获取店铺的营业状态")
    public Result getShopStatus() {
        Integer status = (Integer) redisTemplate.opsForValue().get(SHOP_STATUS);
        log.info("查询店铺营业状态：{}", status == 1 ? "营业中" : "打样中");
        return Result.success(status);
    }

    @PutMapping("/{status}")
    @ApiOperation("设置营业状态")
    public Result setShopStatus(@PathVariable Integer status) {
        log.info("设置店铺营业状态：{}", status);
        redisTemplate.opsForValue().setIfAbsent(SHOP_STATUS, StatusConstant.SHOP_OPEN);  //店铺默认营业
        redisTemplate.opsForValue().set(SHOP_STATUS, status);
        return Result.success();
    }
}
