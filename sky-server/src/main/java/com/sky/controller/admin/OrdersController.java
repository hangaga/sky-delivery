package com.sky.controller.admin;

import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersConfirmDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrdersService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController("adminOrders")
@RequestMapping("/admin/order")
@Api(tags = "订单管理接口")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @GetMapping("/conditionSearch")
    @ApiOperation("分页查询订单")
    public Result conditionSearch(OrdersPageQueryDTO ordersPageQueryDTO) {
        log.info("动态条件分页查询订单的接口：{}", ordersPageQueryDTO);
        PageResult pageResult = ordersService.conditionSearch(ordersPageQueryDTO);
        return Result.success(pageResult);
    }


    @GetMapping("/statistics")
    @ApiOperation("统计各个状态的订单的数量")
    public Result<OrderStatisticsVO> statistics() {
        log.info("统计各个状态的订单的数量");
        OrderStatisticsVO orderStatisticsVO = ordersService.statisticsByStatus();
        return Result.success(orderStatisticsVO);
    }


    @GetMapping("/details/{id}")
    @ApiOperation("根据ID查询订单详情")
    public Result<OrderVO> detail(@PathVariable Long id){
        log.info("根据ID查询订单详情：{}",id);
        OrderVO orderVO = ordersService.getOrdersInfoById(id);
        return Result.success(orderVO);
    }


    @PutMapping("/confirm")
    @ApiOperation("接单")
    public Result confirm(@RequestBody OrdersConfirmDTO ordersConfirmDTO){
        log.info("接单：{}",ordersConfirmDTO);
        ordersService.confirmById(ordersConfirmDTO);
        return Result.success();
    }

    @PutMapping("/rejection")
    @ApiOperation("拒单")
    public Result rejection(@RequestBody OrdersRejectionDTO ordersRejectionDTO){
        log.info("拒单：{}",ordersRejectionDTO);
        ordersService.rejection(ordersRejectionDTO);
        return Result.success();
    }


    @PutMapping("/cancel")
    @ApiOperation("取消订单")
    public Result cancel(@RequestBody OrdersCancelDTO ordersCancelDTO){
        log.info("取消订单：{}",ordersCancelDTO);
        ordersService.cancel(ordersCancelDTO);
        return Result.success();
    }

    @PutMapping("/delivery/{id}")
    @ApiOperation("派送指定订单")
    public Result delivery(@PathVariable Long id){
        log.info("派送指定订单：{}",id);
        ordersService.delivery(id);
        return Result.success();
    }


    @PutMapping("/complete/{id}")
    @ApiOperation("完成指定的订单")
    public Result complete(@PathVariable Long id){
        log.info("完成指定的订单：{}",id);
        ordersService.complete(id);
        return Result.success();
    }


}
