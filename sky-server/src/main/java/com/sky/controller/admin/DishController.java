package com.sky.controller.admin;

import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.dto.DishDTO;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin/dish")
@Api(tags = "菜品管理")
public class DishController {

    @Autowired
    private DishService dishService;

    @PostMapping
    @ApiOperation("新增菜品")
    public Result add(@RequestBody DishDTO dishDTO) {
        log.info("新增菜品:{}", dishDTO);
        dishService.insertDishWithFlavors(dishDTO);
        return Result.success();
    }


    @GetMapping("/page")
    @ApiOperation("菜品分页查询")
    public Result page(DishPageQueryDTO dishPageQueryDTO) {
        log.info("菜品信息分页查询：{}", dishPageQueryDTO);
        PageResult pageResult = dishService.page(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    @DeleteMapping
    @ApiOperation("批量删除菜品")
    public Result delete(@RequestParam List<Long> ids) {
        log.info("批量删除菜品:{}", ids);
        dishService.deleteByIds(ids);
        return Result.success();
    }


    @GetMapping("/{id}")
    @ApiOperation("根据ID查询菜品信息")
    public Result findById(@PathVariable Long id) {
        log.info("根据id查询菜品,包含口味信息:{}", id);
        DishVO dishVO = dishService.findById(id);
        return Result.success(dishVO);
    }

    @PutMapping
    @ApiOperation("修改菜品")
    public Result update(@RequestBody DishDTO dishDTO) {
        log.info("修改菜品：{}", dishDTO);
        dishService.update(dishDTO);
        return Result.success();
    }

    @PutMapping("/status/{status}/{id}")
    @ApiOperation("菜品起售、停售")
    public Result enableOrDisenable(@PathVariable Integer status, @PathVariable Long id) {
        log.info("根据菜品id修改起售、停售：status={},id={}", status, id);
        dishService.enableOrDisenable(status, id);
        return Result.success();
    }

    @GetMapping("/list")
    @ApiOperation("条件查询菜品列表")
    public Result<List<Dish>> listByInfo(DishPageQueryDTO dishPageQueryDTO) {
        log.info("根据分类ID、菜品名称查询起售的菜品列表 :{}", dishPageQueryDTO);
        List<Dish> list = dishService.listByInfo(dishPageQueryDTO);
        return Result.success(list);
    }

}
