package com.sky.controller.user;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/shoppingCart")
@Api(tags = "C端-购物车相关接口")
public class ShoppingCardController {

    @Autowired
    private ShoppingCardService shoppingCardService;

    @PostMapping("/add")
    @ApiOperation("添加购物车")
    public Result add(@RequestBody ShoppingCartDTO shoppingCartDTO) {
        log.info("往购物车中添加商品：{}", shoppingCartDTO);
        shoppingCardService.insert(shoppingCartDTO);
        return Result.success();
    }

    @GetMapping("/list")
    @ApiOperation("查看购物车")
    public Result list() {
        log.info("查询当前用户的购物车数据");
        List<ShoppingCart> shoppingCartList = shoppingCardService.list();
        return Result.success(shoppingCartList);
    }


    @PostMapping("/sub")
    @ApiOperation("删除购物车一个商品")
    public Result delete(@RequestBody ShoppingCartDTO shoppingCartDTO) {
        log.info("删除当前用户购物车一个数据：{}",shoppingCartDTO);
        shoppingCardService.sub(shoppingCartDTO);
        return Result.success();
    }

    @DeleteMapping("/clean")
    @ApiOperation("清空购物车")
    public Result clearAll(){
        log.info("清空当前用户购物车:id={}",BaseContext.getCurrentId());
        shoppingCardService.clear();
        return Result.success();
    }

}
