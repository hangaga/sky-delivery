package com.sky.controller.user;


import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController("userShopController")
@RequestMapping("/user/shop/status")
@Api(tags = "C端-店铺操作接口")
public class ShopController {

    private static final String SHOP_STATUS = "shop:status";

    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping
    @ApiOperation("获取营业状态")
    public Result getShopStatus() {
        Integer status = (Integer) redisTemplate.opsForValue().get(SHOP_STATUS);
        log.info("店铺营业状态：{}", status == 1 ? "营业中" : "打样中");
        return Result.success(status);
    }
}
