package com.sky.controller.user;

import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController("userDishList")
@RequestMapping("/user/dish/list")
@Api(tags = "C端-菜品浏览接口")
public class DishController {

    @Autowired
    private DishService dishService;

    @GetMapping
    @ApiOperation("根据分类id查询菜品")
    public Result<List<DishVO>> findDishByCategoryId(Long categoryId){
        log.info("C端-根据分类id查询菜品");
        List<DishVO> list = dishService.findByCategoryId(categoryId);
        return Result.success(list);
    }

}
