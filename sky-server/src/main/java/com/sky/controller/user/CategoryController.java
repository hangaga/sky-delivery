package com.sky.controller.user;

import com.sky.entity.Category;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController("userCateGoryList")
@RequestMapping("/user/category/list")
@Api(tags = "C端-分类接口")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 分类条件查询
     * @param type
     * @return
     */
    @GetMapping
    @ApiOperation("条件查询分类列表")
    public Result<List<Category>> list(Integer type) {
        log.info("条件查询分类列表：type={}", type);
        List<Category> list = categoryService.findByTypeWithEnable(type);
        return Result.success(list);
    }

}
