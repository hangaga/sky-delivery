package com.sky.controller.user;

import com.sky.entity.AddressBook;
import com.sky.result.Result;
import com.sky.service.AddressBookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/addressBook")
@Api(tags = "C端-地址簿接口")
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    @GetMapping("/list")
    @ApiOperation("查询当前用户的地址列表")
    public Result<List<AddressBook>> list() {
        log.info("查询当前用户的地址列表");
        List<AddressBook> addressBookList = addressBookService.list();
        return Result.success(addressBookList);
    }


    @PostMapping
    @ApiOperation("新增当前用户收货地址")
    private Result add(@RequestBody AddressBook addressBook) {
        log.info("新增当前用户收货地址：{}", addressBook);
        addressBookService.insert(addressBook);
        return Result.success();
    }


    @GetMapping("/{id}")
    @ApiOperation("根据当前用户id查询地址")
    public Result<AddressBook> findById(@PathVariable Long id) {
        log.info("根据当前用户id查询地址：{}", id);
        AddressBook addressBook = addressBookService.findById(id);
        return Result.success(addressBook);
    }


    @PutMapping
    @ApiOperation("根据当前用户ID修改地址信息")
    public Result update(@RequestBody AddressBook addressBook) {
        log.info("根据当前用户ID修改地址信息：{}", addressBook);
        addressBookService.update(addressBook);
        return Result.success();
    }


    @DeleteMapping
    @ApiOperation("根据当前用户ID删除地址信息")
    public Result deleteById(Long id) {
        log.info("根据当前用户ID删除地址信息：{}", id);
        addressBookService.deleteById(id);
        return Result.success();
    }


    @PutMapping("/default")
    @ApiOperation("设置默认的收货地址")
    public Result updateDefault(@RequestBody AddressBook addressBook) {
        log.info("设置默认的收货地址：{}", addressBook);
        addressBookService.updateDefault(addressBook);
        return Result.success();
    }


    @GetMapping("/default")
    @ApiOperation("查询当前登录用户默认的收货地址")
    public Result getDefaultAddressBook() {
        log.info("查询当前登录用户默认的收货地址");
        AddressBook addressBook = addressBookService.getDefaultAddressBook();
        return Result.success(addressBook);
    }


}
