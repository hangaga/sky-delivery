package com.sky.handler;

import com.sky.constant.MessageConstant;
import com.sky.exception.BaseException;
import com.sky.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器，处理项目中抛出的业务异常
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 捕获业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result baseExceptionHandler(BaseException ex){
    	ex.printStackTrace();
        log.error("异常信息：{}", ex.getMessage());
        return Result.error(ex.getMessage());
    }


    /**
     * 捕获新增员工时 员工信息已存在
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result baseExceptionHandler(DuplicateKeyException ex){
        ex.printStackTrace();
        String errorMessage = ex.getCause().getMessage();
        log.error("异常信息：{}", errorMessage);
        if(errorMessage.contains("Duplicate entry")){
            errorMessage = errorMessage.split(" ")[2] + "已存在";
        }
        return Result.error(errorMessage);
    }


    /**
     * 捕获其他异常
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(Exception ex){
    	ex.printStackTrace();
        log.error("异常信息：{}", ex.getMessage());
        return Result.error(MessageConstant.UNKNOWN_ERROR);
    }

}
