package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {

    @Select("select * from setmeal where category_id = #{id}")
    List<Setmeal> selectSetmealByCategoryId(Long id);

    //停售套餐、修改套餐
    @AutoFill(OperationType.UPDATE)
    void update(Setmeal setmeal);

    //添加套餐(默认停售)
    @AutoFill(OperationType.INSERT)
    @Options(useGeneratedKeys = true, keyProperty = "id")   //返回id，在添加 套餐菜品关系 时使用
    @Insert("insert into setmeal (name, category_id, price, status, description, image, create_time, update_time, create_user, update_user)" +
            " values (#{name},#{categoryId},#{price},#{status},#{description},#{image},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insert(Setmeal setmeal);

    List<SetmealVO> page(SetmealPageQueryDTO setmealPageQueryDTO);

    Long countSetmealEnableByIds(List<Long> ids);

    void deleteByIds(List<Long> ids);

    @Select("select * from setmeal where id = #{id}")
    Setmeal findById(Long id);

    @Select("select count(*) from setmeal s left join setmeal_dish sd on s.id = sd.setmeal_id" +
            "                    left join dish d on sd.dish_id = d.id" +
            "                    where s.id = #{id} and d.status = 0")
    Long countDisenableDishOfSetmeal(Long id);

    /**
     * C端-根据分类id查询套餐(起售)
     * @param categoryId
     * @return
     */
    List<Setmeal> findEnableSetmealByCategoryId(Long categoryId);

    /**
     * 根据套餐状态 统计数量
     * @param status
     * @return
     */
    @Select("select count(*) from setmeal where status = #{status}")
    Integer countSetmealByStatus(Integer status);
}
