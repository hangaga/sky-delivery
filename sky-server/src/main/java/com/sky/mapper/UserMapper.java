package com.sky.mapper;

import com.sky.dto.UserReportDTO;
import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface UserMapper {

    /**
     * 根据opid获取用户信息
     *
     * @param openid
     * @return
     */
    @Select("select * from user where openid = #{openid}")
    User getByOpenid(String openid);

    /**
     * 插入用户信息
     *
     * @param user
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into user (openid, name, phone, sex, id_number, avatar, create_time) " +
            "values (#{openid},#{name},#{phone},#{sex},#{idNumber},#{avatar},#{createTime})")
    void insert(User user);

    /**
     * 统计 指定时间内 用户新增数量
     * @param begin
     * @param now
     * @return
     */
    List<UserReportDTO> selectAddOfDay(LocalDateTime begin, LocalDateTime now);

    /**
     * 指定时间前的总用户数
     * @param beginDate
     * @return
     */
    @Select("select count(*) from user where create_time < #{beginDate}")
    Integer countTotalByCreateTime(LocalDateTime beginDate);

    /**
     * 指定时间内 新增用户
     * @param begin
     * @param end
     * @return
     */
    @Select("select count(*) from user where create_time between #{begin} and #{end}")
    Integer countByTime(LocalDateTime begin, LocalDateTime end);
}
