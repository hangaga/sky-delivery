package com.sky.mapper;

import com.sky.dto.*;
import com.sky.entity.Orders;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.OrderVO;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface OrdersMapper {
    /**
     * 保存订单
     *
     * @param orders
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into orders (number, status, user_id, address_book_id, order_time, pay_method, pay_status, amount, remark, phone, address, consignee, estimated_delivery_time, delivery_status, delivery_time, pack_amount, tableware_number, tableware_status) " +
            "values (#{number},#{status},#{userId},#{addressBookId},#{orderTime},#{payMethod},#{payStatus},#{amount},#{remark},#{phone},#{address},#{consignee},#{estimatedDeliveryTime},#{deliveryStatus},#{deliveryTime},#{packAmount},#{tablewareNumber},#{tablewareStatus})")
    void insert(Orders orders);


    /**
     * 根据用户id 查出所有订单
     *
     * @param orders
     * @return
     */
    List<OrderVO> select(Orders orders);

    /**
     * 根据ID查询订单详情
     *
     * @param id
     * @return
     */
    @Select("select * from orders where id = #{id}")
    List<OrderVO> selectById(Long id);

    /**
     * 更新订单状态
     *
     * @param orders
     */
    void updateStatus(Orders orders);

    /**
     * 管理端-动态查询订单
     *
     * @param ordersPageQueryDTO
     * @return
     */
    List<OrderVO> selectByInfo(OrdersPageQueryDTO ordersPageQueryDTO);

    @Select("select count(*) from orders where status = #{status}")
    Integer statisticsByStatus(Integer status);

    @Select("select * from orders where status = #{status} and order_time < #{before15MinTime}")
    List<Orders> selectByStatusAndLtTime(Integer status, LocalDateTime before15MinTime);

    List<TurnoverReportDTO> selectTurnover(LocalDateTime beginTime, LocalDateTime endTime, Integer status);


    List<OrderReportDTO> countOrderByOrderTimeAndStatus(LocalDate begin, LocalDate end, Integer status);

    /**
     * 统计 指定时间内 销量排名前10的菜品/套餐数据
     * @param begin
     * @param end
     * @return
     */
    List<SalesReportDTO> top10(LocalDate begin, LocalDate end,Integer status);

    /**
     * 当日营业额
     * @return
     */
    @Select("select ifnull(sum(amount),0) from orders where status = #{status} and order_time between #{begin} and #{end}")
    Double sumAmount(Integer status, LocalDateTime begin,LocalDateTime end);

    /**
     *各种状态的 订单数
     * @param begin
     * @param end
     * @return
     */
    OrderOverViewVO orderCountByStatusOfToday(LocalDateTime begin, LocalDateTime end);

    /**
     * 指定状态和时间内 订单数
     * @param status
     * @param beginToday
     * @param now
     * @return
     */
    Integer orderCountByStatusAndOrderTime(Integer status, LocalDateTime beginToday, LocalDateTime now);

    /**
     * 统计数据
     * @param begin
     * @param end
     * @return
     */
    BusinessDataVO countByTime(LocalDateTime begin, LocalDateTime end);
}

