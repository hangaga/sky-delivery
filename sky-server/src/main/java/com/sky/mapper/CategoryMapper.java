package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryMapper {

    List<Category> list(CategoryPageQueryDTO categoryPageQueryDTO);

    @Select("select * from category where id = #{id}")
    Category findById(Long id);

    @AutoFill(OperationType.UPDATE)
    void update(Category category);

    @AutoFill(OperationType.INSERT)
    void insert(Category category);

    @Delete("delete from category where id = #{id}")
    void deleteById(Long id);

    @Select("select * from category where type = #{type}")
    List<Category> findByType(Integer type);

    /**
     * C端-分类查询 且状态启用
     * @param type
     * @return
     */
    List<Category> findByTypeWithEnable(Integer type);

    @Select("select * from category order by id")
    List<Category> listALl();

}
