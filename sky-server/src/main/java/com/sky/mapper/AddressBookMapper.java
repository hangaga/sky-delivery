package com.sky.mapper;

import com.sky.entity.AddressBook;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AddressBookMapper {
    /**
     * 查询当前用户的地址列表
     *
     * @param addressBook
     * @return
     */
    @Select("select * from address_book where user_id = #{userId}")
    List<AddressBook> list(AddressBook addressBook);

    /**
     * 新增当前用户收货地址
     *
     * @param addressBook
     */
    @Insert("insert into address_book (user_id, consignee, sex, phone, province_code, province_name, city_code, " +
            "city_name, district_code, district_name, detail, label, is_default, create_time) " +
            "values (#{userId},#{consignee},#{sex},#{phone},#{provinceCode},#{provinceName},#{cityCode},#{cityName}," +
            "#{districtCode},#{districtName},#{detail},#{label},#{isDefault},#{createTime})")
    void insert(AddressBook addressBook);

    /**
     * 根据id查询地址
     *
     * @param id
     * @return
     */
    @Select("select * from address_book where id = #{id}")
    AddressBook findById(Long id);

    /**
     * 根据当前用户ID修改地址信息
     *
     * @param addressBook
     */
    void update(AddressBook addressBook);

    /**
     * 根据当前用户ID删除地址信息
     *
     * @param id
     */
    @Delete("delete from address_book where id = #{id}")
    void deleteById(Long id);

    /**
     * 查询当前登录用户默认的收货地址
     *
     * @return
     */
    @Select("select * from address_book where is_default = #{isDefault} and user_id = #{userId}")
    AddressBook getDefaultAddressBook(AddressBook addressBook);

    /**
     * 根据用户ID修改地址的默认状态
     * @param nonDefaultFlag
     */
    void updateDefault(AddressBook nonDefaultFlag);
}
