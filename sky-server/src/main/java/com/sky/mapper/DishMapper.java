package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DishMapper {

    @Select("select * from dish where category_id = #{id}")
    List<Dish> selectDishByCategoryId(Long id);

    @AutoFill(OperationType.INSERT)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into dish (name, category_id, price, image, description, create_time, update_time, create_user, update_user) " +
            " values (#{name},#{categoryId},#{price},#{image},#{description},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insert(Dish dish);

    List<DishVO> list(DishPageQueryDTO dishPageQueryDTO);

    //统计批量删除菜品中 起售菜品的个数
    Long countEnableByIds(List<Long> ids);

    void deleteByIds(List<Long> ids);

    @Select("select * from dish where id = #{id}")
    Dish selectById(Long id);

    @AutoFill(OperationType.UPDATE)
    void update(Dish dish);


    List<Dish> findByInfo(DishPageQueryDTO dishPageQueryDTO);

    /**
     * C端-根据分类id查询菜品 且菜品起售
     * @param categoryId
     * @return
     */
    List<DishVO> selectEnableDishByCategoryId(Long categoryId);

    /**
     * C端-根据套餐ID查询包含的菜品列表
     * @param id
     * @return
     */
    List<DishItemVO> findDishInfoBySetmealId(Long id);

    /**
     * 查询菜品及口味
     * @param dishDTO
     * @return
     */
    List<DishVO> listDishWithFlavors(DishDTO dishDTO);

    /**
     * 根据菜品状态 统计数量
     * @param status
     * @return
     */
    @Select("select count(*) from dish where status = #{status}")
    Integer countDishByStatus(Integer status);
}
