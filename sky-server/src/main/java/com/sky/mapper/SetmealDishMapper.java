package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface SetmealDishMapper {

    //添加套餐-菜品关系
    void insert(List<SetmealDish> setmealDishes);

    void deleteBySetmealIds(List<Long> ids);

    List<SetmealDish> selectBySetmealId(Long id);

    /**
     * //菜品中关联的套餐
     * @param ids
     * @return
     */
    List<Long> findSetmealIdsByDishIds(List<Long> ids);
}
