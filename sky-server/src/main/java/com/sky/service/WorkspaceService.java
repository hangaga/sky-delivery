package com.sky.service;

import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;

public interface WorkspaceService {
    /**
     * 查询当日店铺的运营数据
     * @return
     */
    BusinessDataVO businessData();

    /**
     * 查询今日订单数据
     * @return
     */
    OrderOverViewVO overviewOrders();

    /**
     * 查询当前店铺的菜品总量
     * @return
     */
    DishOverViewVO overviewDishes();

    /**
     * 查询当前店铺的套餐总量
     * @return
     */
    SetmealOverViewVO overviewSetmeals();

}
