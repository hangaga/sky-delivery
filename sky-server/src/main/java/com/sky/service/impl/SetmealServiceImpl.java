package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private SetmealDishMapper setmealDishMapper;

    @CacheEvict(cacheNames = "setmeal:cache", key = "#a0.categoryId")
    @Override
    public void insert(SetmealDTO setmealDTO) {
        //添加套餐
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        if (setmeal != null) {    //防止空指针异常
            setmeal.setStatus(StatusConstant.DISABLE);  //新增套餐默认停售
            setmealMapper.insert(setmeal);
        }

        //添加套餐菜品关系
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        setmealDishes.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmeal.getId());
        });

        setmealDishMapper.insert(setmealDishes);
    }

    @Override
    public PageResult page(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage(), setmealPageQueryDTO.getPageSize());
        List<SetmealVO> list = setmealMapper.page(setmealPageQueryDTO);
        Page<SetmealVO> page = (Page<SetmealVO>) list;
        return new PageResult(page.getTotal(), page.getResult());
    }

    @CacheEvict(cacheNames = "setmeal:cache", allEntries = true)
    @Transactional
    @Override
    public void deleteByIds(List<Long> ids) {
        Long count = setmealMapper.countSetmealEnableByIds(ids);
        if (count > 0) {  //如果包含起售的套餐，抛出异常
            throw new BusinessException(MessageConstant.SETMEAL_ON_SALE);
        }
        setmealMapper.deleteByIds(ids);
        setmealDishMapper.deleteBySetmealIds(ids);
    }

    @Override
    public SetmealVO findById(Long id) {
        //1、根据id查套餐
        Setmeal setmeal = setmealMapper.findById(id);
        //2、根据套餐id 查套餐菜品关系
        List<SetmealDish> setmealDishList = setmealDishMapper.selectBySetmealId(id);
        //3、拼装SetmealVO 返回
        SetmealVO setmealVO = BeanHelper.copyProperties(setmeal, SetmealVO.class);
        if (setmealVO != null) {  //防止空指针异常
            setmealVO.setSetmealDishes(setmealDishList);
        }
        return setmealVO;
    }

    @CacheEvict(cacheNames = "setmeal:cache", allEntries = true)
    @Transactional
    @Override
    public void update(SetmealDTO setmealDTO) {
        //1、修改套餐数据
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        setmealMapper.update(setmeal);
        //2、修改套餐关联的 菜品-套餐数据  （先删、再加）
        setmealDishMapper.deleteBySetmealIds(Collections.singletonList(setmealDTO.getId())); //删

        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();                    //取出浏览器传过来的 套餐菜品数据
        if (!CollectionUtils.isEmpty(setmealDishes)) {
            setmealDishes.forEach(setmealDish -> {
                setmealDish.setSetmealId(setmealDTO.getId());
            }); //这里如果套餐里新增了菜品，需要为其赋值setmeal_id后，再添加到 套餐-菜品表
        }
        setmealDishMapper.insert(setmealDishes);
    }

    @CacheEvict(cacheNames = "setmeal:cache", allEntries = true)
    @Transactional
    @Override
    public void enableOrDisenable(Integer status, Long id) {
        //1、如果套餐内有 停售 菜品 => 不允许起售该套餐（抛出异常）
        Long count = setmealMapper.countDisenableDishOfSetmeal(id);
        if (count > 0) {
            throw new BusinessException(MessageConstant.SETMEAL_ENABLE_FAILED);
        }
        //2、如果套餐中 所有菜品 均起售，则可以 起售/停售 该套餐
        Setmeal setmeal = Setmeal.builder().id(id).status(status).build();
        setmealMapper.update(setmeal);
    }

    /**
     * C端-根据分类id查询套餐(起售)
     *
     * @param categoryId
     * @return
     */
    @Cacheable(cacheNames = "setmeal:cache", key = "#a0")
    @Override
    public List<Setmeal> findEnableSetmealByCategoryId(Long categoryId) {
        List<Setmeal> list = setmealMapper.findEnableSetmealByCategoryId(categoryId);
        return list;
    }

    /**
     * C端-根据套餐ID查询包含的菜品列表
     *
     * @param id
     * @return
     */
    @Autowired
    private DishMapper dishMapper;

    @Override
    public List<DishItemVO> findDishBySetmealId(Long id) {
        List<DishItemVO> list = dishMapper.findDishInfoBySetmealId(id);
        return list;
    }
}
