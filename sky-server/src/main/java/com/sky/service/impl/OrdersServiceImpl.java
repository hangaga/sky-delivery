package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.*;
import com.sky.entity.AddressBook;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.entity.ShoppingCart;
import com.sky.exception.BaseException;
import com.sky.mapper.AddressBookMapper;
import com.sky.mapper.OrderDetailMapper;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.ShoppingCardMapper;
import com.sky.result.PageResult;
import com.sky.server.WebSocketServer;
import com.sky.service.OrdersService;
import com.sky.utils.BeanHelper;
import com.sky.utils.BaiduMapUtils;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private AddressBookMapper addressBookMapper;

    @Autowired
    private ShoppingCardMapper shoppingCardMapper;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Autowired
    private BaiduMapUtils baiduMapUtils;

    @Value("${sky.shop.address}")
    private String address;

    @Autowired
    private WebSocketServer webSocketServer;

    /**
     * 用户下单
     *
     * @param ordersSubmitDTO
     * @return
     */
    @Override
    public OrderSubmitVO submit(OrdersSubmitDTO ordersSubmitDTO) throws Exception {
        //1、地址为空，不能下单（抛异常）
        AddressBook addressBook = addressBookMapper.findById(ordersSubmitDTO.getAddressBookId());
        if (addressBook == null) {
            throw new BaseException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }
        //2、购物车为空，不能下单（抛异常）
        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();
        List<ShoppingCart> shoppingCarts = shoppingCardMapper.list(shoppingCart);
        if (CollectionUtils.isEmpty(shoppingCarts)) {
            throw new BaseException(MessageConstant.SHOPPING_CART_IS_NULL);
        }

        //校验配送范围，如果用户的【收货地址】距离【商家门店】超出配送范围（配送范围为5公里内），则下单失败，提示错误信息。
        //拼接收货地址
        System.out.println("===================校验配送距离=======================");
        String destination = addressBook.getProvinceName() + addressBook.getCityName() + addressBook.getDistrictName() + addressBook.getDetail();
        //获取起始地和终点的经纬度
        String originLat = baiduMapUtils.getLat(address);
        String originLng = baiduMapUtils.getLng(address);
        String destinationLat = baiduMapUtils.getLat(destination);
        String destinationLng = baiduMapUtils.getLng(destination);
        //获取两地距离
        Long distance = baiduMapUtils.getDistance(originLat, originLng, destinationLat, destinationLng);
        if(distance > 5000){//超出配送范围（5公里）
            throw new BaseException(MessageConstant.DELIVERY_OUT_OF_RANGE);
        }


        //3、保存订单信息  订单号（IdWorker雪花算法 / 当前系统时间）
        Orders orders = BeanHelper.copyProperties(ordersSubmitDTO, Orders.class);

        orders.setNumber(String.valueOf(System.nanoTime()));
        orders.setStatus(Orders.ORDER_STAUTS_PENDING_PAYMENT);//待付款
        orders.setUserId(BaseContext.getCurrentId());
        orders.setAddressBookId(addressBook.getId());
        orders.setOrderTime(LocalDateTime.now());
        orders.setPayStatus(Orders.PAY_STATUS_UN_PAID);//未支付

        orders.setPhone(addressBook.getPhone());
        orders.setAddress(addressBook.getDetail());
        orders.setConsignee(addressBook.getConsignee());

        ordersMapper.insert(orders);

        //4、保存订单明细
        List<OrderDetail> orderDetailList = shoppingCarts.stream().map(cart -> {
            OrderDetail orderDetail = BeanHelper.copyProperties(cart, OrderDetail.class);
            orderDetail.setOrderId(orders.getId());
            return orderDetail;
        }).collect(Collectors.toList());
        orderDetailMapper.insertBatch(orderDetailList);

        //提醒商家，有新的订单
        Map map = new HashMap();
        map.put("type",1);
        map.put("orderId",orders.getId());
        map.put("content","订单号"+orders.getNumber());
        webSocketServer.sendMessageToAllClient(JSON.toJSONString(map));


        //5、清空购物车
        shoppingCardMapper.delete(shoppingCart);

        //6、组装数据返回
        OrderSubmitVO orderSubmitVO = OrderSubmitVO.builder()
                .id(orders.getId())
                .orderNumber(orders.getNumber())
                .orderAmount(orders.getAmount())
                .orderTime(orders.getOrderTime())
                .build();
        return orderSubmitVO;
    }

    /**
     * 历史订单查询
     *
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult list(OrdersPageQueryDTO ordersPageQueryDTO) {
        //0、分页
        PageHelper.startPage(ordersPageQueryDTO.getPage(), ordersPageQueryDTO.getPageSize());
        //1、根据用户id 查出所有订单
        Orders orders = Orders.builder().userId(BaseContext.getCurrentId()).status(ordersPageQueryDTO.getStatus()).build();
        List<OrderVO> ordersList = ordersMapper.select(orders);
        //2、根据每个订单 查订单明细 组装数据
        for (OrderVO orderVO : ordersList) {
            List<OrderDetail> orderDetailList = orderDetailMapper.selectByOrderId(orderVO.getId());
            orderVO.setOrderDetailList(orderDetailList);
        }
        //3、返回数据
        Page page = (Page) ordersList;
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 根据ID查询订单详情
     *
     * @param id
     * @return
     */
    @Override
    public OrderVO getOrdersInfoById(Long id) {
        //1、根据订单id查询当前用户的订单
        List<OrderVO> ordersList = ordersMapper.selectById(id);
        //2、组装数据
        OrderVO orderVO = null;
        if (!CollectionUtils.isEmpty(ordersList)) {
            orderVO = ordersList.get(0);
            //orderVO.setOrderDishes();
            orderVO.setOrderDetailList(orderDetailMapper.selectByOrderId(orderVO.getId()));
        }
        return orderVO;
    }

    /**
     * +
     * 用户取消订单接口
     *
     * @param id
     */
    @Override
    public void cancelById(Long id) {
        List<OrderVO> orderVOList = ordersMapper.selectById(id);
        OrderVO orderVO = orderVOList.get(0);
        //1、订单是否存在
        if (orderVO == null) { //订单不存在
            throw new BaseException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //2、校验订单状态
        Integer status = orderVO.getStatus();
        Integer payStatus = orderVO.getPayStatus();

        if (status > 2) {
            throw new BaseException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //待接单（退款、修改支付状态）
        if (Orders.ORDER_STAUTS_TO_BE_CONFIRMED.equals(status)) {
            //退款逻辑。。
            orderVO.setPayStatus(Orders.PAY_STATUS_REFUND);
        }
        //待接单、待接单(修改订单状态、取消时间、取消原因)
        ordersMapper.updateStatus(Orders.builder().id(id)
                .status(Orders.ORDER_STAUTS_CANCELLED)
                .cancelTime(LocalDateTime.now())
                .cancelReason("用户取消").build());
    }

    /**
     * 再来一单
     *
     * @param id
     */
    @Override
    public void repetition(Long id) {
        //1、先根据订单id查询订单明细
        List<OrderDetail> orderDetailList = orderDetailMapper.selectByOrderId(id);
        if (!CollectionUtils.isEmpty(orderDetailList)) {
            //2、组装数据
            List<ShoppingCart> shoppingCarts = orderDetailList.stream().map(orderDetail -> {
                ShoppingCart shoppingCart = BeanHelper.copyProperties(orderDetail, ShoppingCart.class);
                shoppingCart.setUserId(BaseContext.getCurrentId());
                shoppingCart.setCreateTime(LocalDateTime.now());
                return shoppingCart;
            }).collect(Collectors.toList());
            //3、重新加入购物车（批量）
            shoppingCardMapper.insertBatch(shoppingCarts);
        }
    }


    /**
     * 管理端-订单搜索
     *
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult conditionSearch(OrdersPageQueryDTO ordersPageQueryDTO) {
        //1、分页
        PageHelper.startPage(ordersPageQueryDTO.getPage(), ordersPageQueryDTO.getPageSize());
        //2、查询订单
        List<OrderVO> orderVOList = ordersMapper.selectByInfo(ordersPageQueryDTO);
        //3、组装数据，orderDishes ："菜名1*n;菜名2*n;菜名3*n;"
        for (OrderVO orderVO : orderVOList) {
            List<OrderDetail> orderDetailList = orderDetailMapper.selectByOrderId(orderVO.getId());
            String orderDishes = "";
            for (OrderDetail orderDetail : orderDetailList) {
                orderDishes += orderDetail.getName() + "*" + orderDetail.getNumber() + ";";
            }
            orderVO.setOrderDishes(orderDishes);
        }
        Page p = (Page) orderVOList;
        return new PageResult(p.getTotal(), p.getResult());
    }

    /**
     * 统计各个状态的订单的数量
     *
     * @return
     */
    @Override
    public OrderStatisticsVO statisticsByStatus() {
        OrderStatisticsVO orderStatisticsVO = OrderStatisticsVO.builder()
                .toBeConfirmed(ordersMapper.statisticsByStatus(Orders.ORDER_STAUTS_TO_BE_CONFIRMED))
                .confirmed(ordersMapper.statisticsByStatus(Orders.ORDER_STAUTS_CONFIRMED))
                .deliveryInProgress(ordersMapper.statisticsByStatus(Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS))
                .build();
        return orderStatisticsVO;
    }


    /**
     * 接单
     *
     * @param ordersConfirmDTO
     */
    @Override
    public void confirmById(OrdersConfirmDTO ordersConfirmDTO) {
        //1、更新订单的状态为 **已接单**
        Orders orders = Orders.builder().id(ordersConfirmDTO.getId()).status(Orders.ORDER_STAUTS_CONFIRMED).build();
        ordersMapper.updateStatus(orders);
    }

    /**
     * 拒单
     *
     * @param ordersRejectionDTO
     */
    @Override
    public void rejection(OrdersRejectionDTO ordersRejectionDTO) {
        //1、订单存在 且状态为待接单（ORDER_STAUTS_TO_BE_CONFIRMED），才可以拒单
        List<OrderVO> orderVOList = ordersMapper.selectById(ordersRejectionDTO.getId());
        if (CollectionUtils.isEmpty(orderVOList)) {
            throw new BaseException(MessageConstant.ORDER_NOT_FOUND);
        }
        OrderVO orderVO = orderVOList.get(0);
        if (orderVO.getStatus() != 2) {
            throw new BaseException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //2、如果订单已支付，要退款，并修改支付状态为退款（PAY_STATUS_REFUND）
        if (Orders.PAY_STATUS_PAID.equals(orderVO.getPayStatus())) {
            //退款。。
            //修改支付状态
            Orders orders = Orders.builder().id(orderVO.getId()).payStatus(Orders.PAY_STATUS_REFUND).build();
            ordersMapper.updateStatus(orders);
        }
        //3、更新订单状态 已取消（ORDER_STAUTS_CANCELLED）,订单拒绝原因 rejectionReason
        Orders orders = Orders.builder()
                .id(orderVO.getId())
                .status(Orders.ORDER_STAUTS_CANCELLED)
                .rejectionReason(ordersRejectionDTO.getRejectionReason())
                .build();
        ordersMapper.updateStatus(orders);
    }


    /**
     * 取消订单
     *
     * @param ordersCancelDTO
     */
    @Override
    public void cancel(OrdersCancelDTO ordersCancelDTO) {
        List<OrderVO> orderVOList = ordersMapper.selectById(ordersCancelDTO.getId());
        //1、订单是否存在
        if (CollectionUtils.isEmpty(orderVOList)) {
            throw new BaseException(MessageConstant.ORDER_NOT_FOUND);
        }
        OrderVO orderVO = orderVOList.get(0);
        //2、订单已支付，需要退款，修改支付状态 为退款
        if (Orders.PAY_STATUS_PAID.equals(orderVO.getPayStatus())) {
            //退款。。
            //修改支付状态
            Orders orders = Orders.builder().id(orderVO.getId()).payStatus(Orders.PAY_STATUS_REFUND).build();
            ordersMapper.updateStatus(orders);
        }
        //3、更新订单状态 已取消
        Orders orders = Orders.builder()
                .id(orderVO.getId())
                .status(Orders.ORDER_STAUTS_CANCELLED)
                .cancelReason(ordersCancelDTO.getCancelReason())
                .cancelTime(LocalDateTime.now())
                .build();
        ordersMapper.updateStatus(orders);
    }

    /**
     * 派送订单
     *
     * @param id
     */
    @Override
    public void delivery(Long id) {
        //1、订单是否存在 只有已结单 才可以进行派送
        List<OrderVO> orderVOList = ordersMapper.selectById(id);
        if (CollectionUtils.isEmpty(orderVOList)) {
            throw new BaseException(MessageConstant.ORDER_NOT_FOUND);
        }
        OrderVO orderVO = orderVOList.get(0);
        if (!Orders.ORDER_STAUTS_CONFIRMED.equals(orderVO.getStatus())) {
            throw new BaseException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //2、更新订单状态 派送中
        ordersMapper.updateStatus(Orders.builder().id(id).status(Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS).build());
    }


    /**
     * 完成订单
     *
     * @param id
     */
    @Override
    public void complete(Long id) {
        //1、订单是否存在
        List<OrderVO> orderVOList = ordersMapper.selectById(id);
        if (CollectionUtils.isEmpty(orderVOList)) {
            throw new BaseException(MessageConstant.ORDER_NOT_FOUND);
        }
        //2、只有派送中的订单，才可完成订单
        OrderVO orderVO = orderVOList.get(0);
        if (!Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS.equals(orderVO.getStatus())) {
            throw new BaseException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //3、更新状态
        ordersMapper.updateStatus(Orders.builder().id(id).status(Orders.ORDER_STAUTS_COMPLETED).build());
    }


    /**
     * 用户催单
     * @param id
     */
    @Override
    public void reminder(Long id) throws Exception {
        List<OrderVO> orderVOList = ordersMapper.selectById(id);
        if(!CollectionUtils.isEmpty(orderVOList)){
            OrderVO orderVO = orderVOList.get(0);
            Map map = new HashMap();
            map.put("type",2);
            map.put("roderId",orderVO.getId());
            map.put("content","订单号:"+orderVO.getNumber());
            webSocketServer.sendMessageToAllClient(JSON.toJSONString(map));
        }
    }
}
