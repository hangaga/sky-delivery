package com.sky.service.impl;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.vo.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 统计指定时间段中每一天的营业额
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {
        //1、获取日期列表 dateList
        List<String> dateList = getDateList(begin, end);

        //2、统计指定日期范围的营业额列表 turnoverList
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

        List<TurnoverReportDTO> turnoverReportVOList = ordersMapper.selectTurnover(beginTime, endTime, Orders.ORDER_STAUTS_COMPLETED);
        Map<String, BigDecimal> TurnoverReportDTOMap = turnoverReportVOList.stream().collect(Collectors.toMap(TurnoverReportDTO::getOrderDate, TurnoverReportDTO::getOrderMoney));
        List<BigDecimal> turnoverList = dateList.stream().map(date -> {
            return TurnoverReportDTOMap.get(date) == null ? new BigDecimal("0") : TurnoverReportDTOMap.get(date);
        }).collect(Collectors.toList());

        return new TurnoverReportVO(dateList, turnoverList);
    }

    /**
     * 统计 指定时间内 用户新增数量及总数量
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {
        //1、日期列表 dateList
        List<String> dateList = getDateList(begin, end);

        //2、新增用户数列表 newUserList
        LocalDateTime beginDate = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endDate = LocalDateTime.of(end, LocalTime.MAX);
        List<UserReportDTO> userReportDTOList = userMapper.selectAddOfDay(beginDate, endDate);
        Map<String, Integer> dateMap = userReportDTOList.stream().collect(Collectors.toMap(UserReportDTO::getCreateDate, UserReportDTO::getUserCount));
        List<Integer> newUserList = dateList.stream().map(date -> dateMap.get(date) == null ? 0 : dateMap.get(date)).collect(Collectors.toList());

        //3、总用户量列表 totalUserList
        Integer baseCount = userMapper.countTotalByCreateTime(beginDate);
        List<Integer> totalUserList = newUserList.stream().map(num -> num += baseCount).collect(Collectors.toList());

        return new UserReportVO(dateList, totalUserList, newUserList);
    }


    /**
     * 统计 指定时间内 订单的总量、有效订单量、有效订单率等数据
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public OrderReportVO ordersStatistics(LocalDate begin, LocalDate end) {
        //1、日期列表 dateList
        List<String> dateList = getDateList(begin, end);

        //2、每日订单数列表 orderCountList
        List<OrderReportDTO> orderReportDTOList = ordersMapper.countOrderByOrderTimeAndStatus(begin, end, null);
        Map<String, Integer> orderMap = orderReportDTOList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> orderCountList = dateList.stream().map(date -> orderMap.get(date) == null ? 0 : orderMap.get(date)).collect(Collectors.toList());

        //3、每日有效订单数列表 validOrderCountList
        List<OrderReportDTO> validOrderReportDTOList = ordersMapper.countOrderByOrderTimeAndStatus(begin, end, Orders.ORDER_STAUTS_COMPLETED);
        Map<String, Integer> validOrderMap = validOrderReportDTOList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> validOrderCountList = dateList.stream().map(date -> validOrderMap.get(date) == null ? 0 : validOrderMap.get(date)).collect(Collectors.toList());

        //4、订单总数列表 totalOrderCount
        Integer totalOrderCount = orderCountList.stream().reduce(Integer::sum).get();

        //5、有效订单数列表 validOrderCount
        Integer validOrderCount = validOrderCountList.stream().reduce(Integer::sum).get();

        //6、订单完成率 orderCompletionRate
        Double orderCompletionRate = validOrderCount.doubleValue() / totalOrderCount;
        return new OrderReportVO(dateList, orderCountList, validOrderCountList, totalOrderCount, validOrderCount, orderCompletionRate);
    }

    /**
     * 统计 指定时间内 销量排名前10的菜品/套餐数据
     * @param begin
     * @param end
     * @return
     */
    @Override
    public SalesTop10ReportVO top10(LocalDate begin, LocalDate end) {
        //1、商品名称列表 nameList
        List<SalesReportDTO> salesReportDTOList = ordersMapper.top10(begin, end,Orders.ORDER_STAUTS_COMPLETED);
        //2、销量列表 numberList
        List<String> nameList = salesReportDTOList.stream().map(SalesReportDTO::getGoodsName).collect(Collectors.toList());
        List<Integer> numberList = salesReportDTOList.stream().map(SalesReportDTO::getGoodsNumber).collect(Collectors.toList());

        return new SalesTop10ReportVO(nameList,numberList);
    }

    /**
     * 获取指定日期内 日期列表
     * @param begin
     * @param end
     * @return
     */
    private List<String> getDateList(LocalDate begin, LocalDate end) {
        List<LocalDate> localDateList = begin.datesUntil(end.plusDays(1)).collect(Collectors.toList());
        return localDateList.stream().map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());
    }

    @Autowired
    private HttpServletResponse response;
    /**
     * 导出近30天的数据
     */
    @Override
    public void export() throws Exception {
        //1、加载Excel模板
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("templates/运营数据报表模板.xlsx");
        Workbook workbook = new XSSFWorkbook(in);
        Sheet sheet = workbook.getSheetAt(0);

        //2、加载数据 - 概览数据 30天总数据
        LocalDate beginDate = LocalDate.now().minusDays(30);
        LocalDate endDate = LocalDate.now().minusDays(1);

        LocalDateTime begin = LocalDateTime.of(beginDate, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(endDate, LocalTime.MAX);

        BusinessDataVO businessDataVO = ordersMapper.countByTime(begin,end);
        Integer newUsers = userMapper.countByTime(begin,end);

        //3、定位单元格 填充
        sheet.getRow(1).getCell(1).setCellValue("时间范围：" + beginDate.toString() + "到" + endDate.toString());
        sheet.getRow(3).getCell(2).setCellValue(businessDataVO.getTurnover());
        sheet.getRow(3).getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
        sheet.getRow(3).getCell(6).setCellValue(newUsers);
        sheet.getRow(4).getCell(2).setCellValue(businessDataVO.getValidOrderCount());
        sheet.getRow(4).getCell(4).setCellValue(businessDataVO.getUnitPrice());

        //加载数据 - 明细数据 每一天数据 => 填充
        for (int i = 0; i < 30; i++) {
            LocalDate dailyDate = beginDate.plusDays(i);
            BusinessDataVO _businessDataVO = ordersMapper.countByTime(LocalDateTime.of(dailyDate, LocalTime.MIN), LocalDateTime.of(dailyDate, LocalTime.MAX));
            Integer _newUsers = userMapper.countByTime(LocalDateTime.of(dailyDate, LocalTime.MIN), LocalDateTime.of(dailyDate, LocalTime.MAX));
            Row row = sheet.getRow(7 + i);
            row.getCell(1).setCellValue(dailyDate.toString());
            row.getCell(2).setCellValue(_businessDataVO.getTurnover());
            row.getCell(3).setCellValue(_businessDataVO.getValidOrderCount());
            row.getCell(4).setCellValue(_businessDataVO.getOrderCompletionRate());
            row.getCell(5).setCellValue(_businessDataVO.getUnitPrice());
            row.getCell(6).setCellValue(_newUsers);
        }

        //4、下载 - 流
        ServletOutputStream out = response.getOutputStream();
        workbook.write(out);

        //5、释放资源
        workbook.close();
        out.close();
    }
}
