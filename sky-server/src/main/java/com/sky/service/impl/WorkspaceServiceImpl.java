package com.sky.service.impl;

import com.sky.constant.StatusConstant;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.mapper.DishMapper;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.WorkspaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.*;
import java.util.List;

@Service
public class WorkspaceServiceImpl implements WorkspaceService {

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;
    /**
     * 查询当日店铺的运营数据
     *
     * @return
     */
    @Override
    public BusinessDataVO businessData() {
        //0、今日时间
        LocalDateTime beginToday = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        LocalDateTime now = LocalDateTime.now();

        //1、营业额 turnover Double
        Double turnover = ordersMapper.sumAmount(Orders.ORDER_STAUTS_COMPLETED,beginToday, now);
        //2、有效订单数 validOrderCount Integer
        Integer validOrderCount = ordersMapper.orderCountByStatusAndOrderTime(Orders.ORDER_STAUTS_COMPLETED, beginToday, now);
        //3、订单完成率 orderCompletionRate Double
        Integer allOrderCount = ordersMapper.orderCountByStatusAndOrderTime(null, beginToday, now);
        if(validOrderCount == 0 || allOrderCount == 0){ //今日无订单 或 无有效订单，则营业额、有效订单、订单完成率、平均客单价均应为0
            List<UserReportDTO> userReportDTOList = userMapper.selectAddOfDay(beginToday, now);
            Integer userCount = 0;
            if(!CollectionUtils.isEmpty(userReportDTOList)){
                userCount = userReportDTOList.get(0).getUserCount();
            }
            return BusinessDataVO.builder()
                    .turnover(0D)
                    .validOrderCount(0)
                    .orderCompletionRate(0D)
                    .unitPrice(0D)
                    .newUsers(userCount)
                    .build();
        }
        //今日有订单 即 allOrderCount 不为 0
        Double orderCompletionRate = validOrderCount.doubleValue() / allOrderCount;
        //4、平均客单价 unitPrice Double
        Double unitPrice = turnover / validOrderCount;
        BigDecimal bigDecimal = new BigDecimal(unitPrice);
        unitPrice = bigDecimal.setScale(2, RoundingMode.HALF_UP).doubleValue();
        //5、新增用户数 newUsers Integer
        List<UserReportDTO> list = userMapper.selectAddOfDay(beginToday, now);
        Integer newUsers = 0;
        if (!CollectionUtils.isEmpty(list)) {
            newUsers = list.get(0).getUserCount();
        }
        return new BusinessDataVO(turnover, validOrderCount, orderCompletionRate, unitPrice, newUsers);
    }

    /**
     * 查询今日订单数据
     * @return
     */
    @Override
    public OrderOverViewVO overviewOrders() {
        //0、今日时间
        LocalDateTime begin = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        LocalDateTime end = LocalDateTime.now();

        //1、待接单数量 waitingOrders 、待派送数量 deliveredOrders、已完成数量 completedOrders、已取消数量 cancelledOrders、全部订单 allOrders
        return ordersMapper.orderCountByStatusOfToday(begin,end);
    }

    /**
     * 查询当前店铺的菜品总量
     * @return
     */
    @Override
    public DishOverViewVO overviewDishes() {
        //1、已启售数量 sold
        Integer sold = dishMapper.countDishByStatus(StatusConstant.ENABLE);
        //2、已停售数量 discontinued
        Integer discontinued = dishMapper.countDishByStatus(StatusConstant.DISABLE);
        return new DishOverViewVO(sold,discontinued);
    }

    /**
     * 查询当前店铺的套餐总量
     * @return
     */
    @Override
    public SetmealOverViewVO overviewSetmeals() {
        //1、已启售数量 sold
        Integer sold = setmealMapper.countSetmealByStatus(StatusConstant.ENABLE);
        //2、已停售数量 discontinued
        Integer discontinued = setmealMapper.countSetmealByStatus(StatusConstant.DISABLE);
        return new SetmealOverViewVO(sold,discontinued);
    }
}
