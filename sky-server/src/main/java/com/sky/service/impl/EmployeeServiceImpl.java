package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.exception.BusinessException;
import com.sky.exception.DataException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final String LOGIN_PASSWORD_ERROR_KEY = "login:error:";//错误标记key
    private static final String LOGIN_LOCK_ERROR_KEY = "login:lock:";//账户锁定key

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //先判断账户是否被锁
        validateAccountLock(username);

        //1、根据用户名查询员工信息
        Employee employee = employeeMapper.findByUsername(username);
        //2、判断用户是否存在
        if (employee == null) {
            log.info("查询到的员工信息为空，返回错误信息");
            throw new DataException(MessageConstant.ACCOUNT_NOT_FOUND);
        }
        //3、先对 页面传过来的密码 进行【加密】； 再校验密码，密码不正确返回错误信息
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!password.equals(employee.getPassword())) {
            log.info("密码错误");
            //每次密码错误就记录一次，有效期5分钟
            redisTemplate.opsForValue().set(LOGIN_PASSWORD_ERROR_KEY + username + ":" + getKey(), "-", 5, TimeUnit.MINUTES);
            Set keys = redisTemplate.keys(LOGIN_PASSWORD_ERROR_KEY + username + ":*");
            //登录密码错误5次，锁定账户一小时
            if (keys != null && keys.size() >= 5) {
                log.info("员工登录5分钟内，错误次数超过5次，锁定账号一小时");
                redisTemplate.opsForValue().set(LOGIN_LOCK_ERROR_KEY + username, "-", 1, TimeUnit.HOURS);
                throw new BusinessException(MessageConstant.LOGIN_LOCK_ERROR);
            }
            throw new BusinessException(MessageConstant.PASSWORD_ERROR);
        }
        //4、用户是否被禁用
        if (StatusConstant.DISABLE.equals(employee.getStatus())) {
            log.info("登录账号{}被锁定，禁止登录", employeeLoginDTO.getUsername());
            throw new BusinessException(MessageConstant.ACCOUNT_LOCKED);
        }
        return employee;
    }


    /**
     * 判断账户是否被锁
     *
     * @param username
     */
    private void validateAccountLock(String username) {
        Object flag = redisTemplate.opsForValue().get(LOGIN_LOCK_ERROR_KEY + username);
        if (ObjectUtils.isNotEmpty(flag)) {   //账户被锁定
            log.info("账户已被锁，不能登录");
            Long expire = redisTemplate.getExpire(LOGIN_LOCK_ERROR_KEY + username);//剩余过期时间
            throw new BusinessException(MessageConstant.LOGIN_LOCK_ERROR_MESSAGE + ",请" + expire + "秒后重试");
        }
    }

    /**
     * 生成5位随机字符串
     *
     * @return
     */
    private String getKey() {
        return RandomStringUtils.randomAlphabetic(5);
    }


    //新增员工
    @Override
    public void insert(EmployeeDTO employeeDTO) {
        //1、补全实体属性
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO, employee);
        //密码加密
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        employee.setStatus(1);

        employee.setCreateUser(BaseContext.getCurrentId());
        employee.setUpdateUser(BaseContext.getCurrentId());
        //2、调用mapper保存到数据库
        employeeMapper.insert(employee);
    }


    //动态查询员工信息
    @Override
    public PageResult list(EmployeePageQueryDTO employeePageQueryDTO) {
        PageHelper.startPage(employeePageQueryDTO.getPage(), employeePageQueryDTO.getPageSize());
        List<Employee> list = employeeMapper.list(employeePageQueryDTO);
        Page<Employee> page = (Page<Employee>) list;
        return new PageResult(page.getTotal(), page.getResult());
    }


    //启用、禁用员工账号
    @Override
    public void updateStatusById(Integer status, Long id) {
        Employee employee = Employee.builder().id(id)
                .status(status)
                .updateTime(LocalDateTime.now())
                .updateUser(BaseContext.getCurrentId())
                .build();
        employeeMapper.updateById(employee);
    }

    @Override
    public Employee selectById(Long id) {
        Employee employee = employeeMapper.selectById(id);
        return employee;
    }

    @Override
    public void updateById(EmployeeDTO employeeDTO) {
        Employee employee = Employee.builder()
                .id(employeeDTO.getId())
                .username(employeeDTO.getUsername())
                .name(employeeDTO.getName())
                .phone(employeeDTO.getPhone())
                .sex(employeeDTO.getSex())
                .idNumber(employeeDTO.getIdNumber())
                .updateTime(LocalDateTime.now())
                .updateUser(BaseContext.getCurrentId())
                .build();
        employeeMapper.updateById(employee);
    }


    @Override
    public void editPassword(PasswordEditDTO passwordEditDTO) {
        String oldPassword = employeeMapper.selectById(passwordEditDTO.getEmpId()).getPassword();
        if (!oldPassword.equals(DigestUtils.md5DigestAsHex(passwordEditDTO.getOldPassword().getBytes()))) { //原始密码不匹配，就抛出异常
            throw new BusinessException(MessageConstant.PASSWORD_EDIT_FAILED);
        }
        //原始密码匹配 => 修改密码
        Employee employee = Employee.builder()
                .id(passwordEditDTO.getEmpId())
                .password(DigestUtils.md5DigestAsHex(passwordEditDTO.getNewPassword().getBytes()))
                .updateTime(LocalDateTime.now())
                .updateUser(BaseContext.getCurrentId())
                .build();
        employeeMapper.updateById(employee);
    }
}
