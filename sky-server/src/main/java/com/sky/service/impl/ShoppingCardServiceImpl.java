package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCardMapper;
import com.sky.service.ShoppingCardService;
import com.sky.utils.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShoppingCardServiceImpl implements ShoppingCardService {

    @Autowired
    private ShoppingCardMapper shoppingCardMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * @param shoppingCartDTO
     */
    @Override
    public void insert(ShoppingCartDTO shoppingCartDTO) {
        //1、先查 当前用户购物车是否存在指定的商品 （菜品-口味，套餐）
        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        List<ShoppingCart> shoppingCartList = shoppingCardMapper.list(shoppingCart);

        //2、如果存在的话  数量+1
        if (!CollectionUtils.isEmpty(shoppingCartList)) {
            ShoppingCart card = shoppingCartList.get(0);
            card.setNumber(card.getNumber() + 1);
            shoppingCardMapper.updateNumberById(card);
        } else {
            //3、如果不存在， 则从数据库查找指定的商品，添加到购物车
            Long dishId = shoppingCart.getDishId();
            if (dishId != null) { //菜品
                Dish dish = dishMapper.selectById(dishId);
                shoppingCart.setName(dish.getName());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setAmount(dish.getPrice());
            } else {  //套餐
                Setmeal setmeal = setmealMapper.findById(shoppingCart.getSetmealId());
                shoppingCart.setName(setmeal.getName());
                shoppingCart.setImage(setmeal.getImage());
                shoppingCart.setAmount(setmeal.getPrice());
            }
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCardMapper.insert(shoppingCart);
        }
    }

    /**
     * 查看当前用户的购物车数据
     * @return
     */
    @Override
    public List<ShoppingCart> list() {
        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();
        List<ShoppingCart> shoppingCartList = shoppingCardMapper.list(shoppingCart);
        return shoppingCartList;
    }

    /**
     * 删除当前用户购物车数据
     * @param shoppingCartDTO
     */
    @Override
    public void sub(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        List<ShoppingCart> shoppingCartList = shoppingCardMapper.list(shoppingCart);

        //先拿到要删除菜品/套餐， 获取当前的数量
        ShoppingCart cart = shoppingCartList.get(0);
        Integer number = cart.getNumber();
        if (number == 1) {  //要删除的菜品/套餐数量为1，直接删
            shoppingCardMapper.deleteById(cart);
        } else {    //要删除的菜品/套餐数量大于1，则使其数量-1
            cart.setNumber(--number);
            shoppingCardMapper.updateNumberById(cart);
        }
    }

    /**
     * 清空当前用户购物车
     */
    @Override
    public void clear() {
        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();
        shoppingCardMapper.delete(shoppingCart);
    }
}
