package com.sky.service.impl;

import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.entity.AddressBook;
import com.sky.mapper.AddressBookMapper;
import com.sky.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AddressBookServiceImpl implements AddressBookService {

    @Autowired
    private AddressBookMapper addressBookMapper;

    /**
     * 查询当前用户的地址列表
     *
     * @return
     */
    @Override
    public List<AddressBook> list() {
        AddressBook addressBook = AddressBook.builder().userId(BaseContext.getCurrentId()).build();
        List<AddressBook> addressBookList = addressBookMapper.list(addressBook);
        return addressBookList;
    }

    /**
     * 新增当前用户收货地址
     *
     * @param addressBook
     */
    @Override
    public void insert(AddressBook addressBook) {
        addressBook.setUserId(BaseContext.getCurrentId());
        addressBook.setCreateTime(LocalDateTime.now());
        addressBook.setIsDefault(0);
        addressBookMapper.insert(addressBook);
    }

    /**
     * 根据id查询地址
     *
     * @param id
     * @return
     */
    @Override
    public AddressBook findById(Long id) {
        AddressBook addressBook = addressBookMapper.findById(id);
        return addressBook;
    }

    /**
     * 根据当前用户ID修改地址信息
     *
     * @param addressBook
     */
    @Override
    public void update(AddressBook addressBook) {
        addressBookMapper.update(addressBook);
    }

    /**
     * 根据当前用户ID删除地址信息
     *
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        addressBookMapper.deleteById(id);
    }

    /**
     * 设置默认的收货地址
     *
     * @param addressBook
     */
    @Override
    public void updateDefault(AddressBook addressBook) {
        //先把当前用户所有地址改为 非默认，
        AddressBook nonDefaultFlag = AddressBook.builder().userId(BaseContext.getCurrentId()).isDefault(0).build();
        addressBookMapper.updateDefault(nonDefaultFlag);
        //再把传进来的地址改为默认地址
        addressBook.setIsDefault(1);
        addressBookMapper.update(addressBook);
    }

    /**
     * 查询当前登录用户默认的收货地址
     *
     * @return
     */
    @Override
    public AddressBook getDefaultAddressBook() {
        AddressBook addressBook = AddressBook.builder().userId(BaseContext.getCurrentId()).isDefault(StatusConstant.ENABLE).build();
        return addressBookMapper.getDefaultAddressBook(addressBook);
    }

}
