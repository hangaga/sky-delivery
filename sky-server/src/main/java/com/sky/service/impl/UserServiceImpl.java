package com.sky.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.BusinessException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import com.sky.vo.UserReportVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    //微信服务接口地址
    private static final String WX_LOGIN_URL = "https://api.weixin.qq.com/sns/jscode2session";

    @Autowired
    private WeChatProperties weChatProperties;

    @Autowired
    private UserMapper userMapper;

    /**
     * 微信登录
     *
     * @param userLoginDTO
     * @return
     */
    @Override
    public User wxlogin(UserLoginDTO userLoginDTO) {
/*
        //1、调用微信接口服务，获取当前微信用户的openid
        Map<String, String> map = new HashMap<>();
        map.put("appid", weChatProperties.getAppid());
        map.put("secret", weChatProperties.getSecret());
        map.put("js_code", userLoginDTO.getCode());
        map.put("grant_type", "authorization_code");
        //返回的是json字符串，需要解析各属性
        String json = HttpClientUtil.doGet(WX_LOGIN_URL, map);
        JSONObject jsonObject = JSON.parseObject(json);
        String openid = jsonObject.getString("openid");
        //2、判断当前openid是否为空，为空则登录失败，抛出异常
        if (!StringUtils.hasLength(openid)) {
            throw new BusinessException(MessageConstant.LOGIN_FAILED);
        }
        //3、判断是否为新用户
        User user = userMapper.getByOpenid(openid);
        //4、如果是新用户，则自动完成注册
        if (user == null) {
            user = User.builder()
                    .openid(openid)
                    .createTime(LocalDateTime.now())
                    .build();
            userMapper.insert(user);
        }
        //5、返回用户对象
        return user;
*/
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("appid",weChatProperties.getAppid());
        paramMap.put("secret", weChatProperties.getSecret());
        paramMap.put("js_code",userLoginDTO.getCode());
        paramMap.put("grant_type","authorization_code");
        String result = HttpClientUtil.doGet(WX_LOGIN_URL, paramMap);
        log.info("微信登录完成：结果：{}",result);
        if(!StringUtils.hasLength(result)){
            throw new BusinessException(MessageConstant.LOGIN_FAILED);
        }
        JSONObject jsonObject = JSONObject.parseObject(result);
        String openid = jsonObject.getString("openid"); //微信用户唯一标识
        if(!StringUtils.hasLength(openid)){
            throw new BusinessException(MessageConstant.LOGIN_FAILED);
        }
        //2、如果用户是第一次访问小程序，则需要完成自动创建（insert）功能
        User user = userMapper.getByOpenid(openid);
        if(user == null){
            user =User.builder().openid(openid).createTime(LocalDateTime.now()).build();
            userMapper.insert(user);
        }
        //3、返回
        return user;
    }

}
