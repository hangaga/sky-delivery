package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class DishServiceImpl implements DishService {

    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private SetmealDishMapper setmealDishMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    @Transactional
    public void insertDishWithFlavors(DishDTO dishDTO) {
        //拷贝属性
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        //1、添加菜品(新增默认停售)
        dish.setStatus(StatusConstant.DISABLE);
        dishMapper.insert(dish);
        //2、添加口味到dish_flavor表，并赋值对应dish_id
        List<DishFlavor> dishFlavors = dishDTO.getFlavors();
        if (!CollectionUtils.isEmpty(dishFlavors)) {
            dishFlavors.stream().forEach(dishFlavor -> dishFlavor.setDishId(dish.getId()));
            dishFlavorMapper.insertFlavorsOfDish(dishFlavors);
        }
        //3、删除Redis缓存中的菜品数据
        cleanCache(dishDTO.getCategoryId().toString());
        log.info("新增菜品，清理缓存中对应的菜品数据");
    }

    /**
     * 清理Redis缓存中指定的Key
     * @param suffix
     */
    private void cleanCache(String suffix) {
        log.info("清理缓存中指定的Key {}", "dish:cache:" + suffix);
        Set<Object> keys = redisTemplate.keys("dish:cache:" + suffix);
        redisTemplate.delete(keys);
    }

    @Override
    public PageResult page(DishPageQueryDTO dishPageQueryDTO) {
        PageHelper.startPage(dishPageQueryDTO.getPage(), dishPageQueryDTO.getPageSize());
        List<DishVO> list = dishMapper.list(dishPageQueryDTO);
        Page<DishVO> page = (Page<DishVO>) list;
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    @Transactional
    public void deleteByIds(List<Long> ids) {
        Long count = dishMapper.countEnableByIds(ids);
        if (count > 0) { //有菜品包含了起售菜品，不能删
            throw new BusinessException(MessageConstant.DISH_ON_SALE);
        }
        List<Long> list = setmealDishMapper.findSetmealIdsByDishIds(ids);
        if (!CollectionUtils.isEmpty(list)) { //有菜品关联了套餐，不能删
            throw new BusinessException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        //行了，该删删吧，连菜品口味一起删了
        dishMapper.deleteByIds(ids);
        dishFlavorMapper.deleteByDishId(ids);
        //菜品缓存全删，查的时候再缓存
        cleanCache("*");
    }

    @Override
    public DishVO findById(Long id) {
        //1、先根据id查菜品
        Dish dish = dishMapper.selectById(id);
        //2、再根据id找菜品口味
        List<DishFlavor> flavorList = dishFlavorMapper.selectByDishId(id);

        //3、组合赋给DishVO返回
        DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);
        //防止空指针异常，判断一下
        if (dishVO != null) {
            dishVO.setFlavors(flavorList);
        }
        return dishVO;
    }

    @Transactional
    @Override
    public void update(DishDTO dishDTO) {
        //1、根据id修改菜品
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        dishMapper.update(dish);
        //2、根据id修改菜品口味信息 => 1）先删、后加
        dishFlavorMapper.deleteByDishId(Collections.singletonList(dish.getId()));   //根据菜品id删口味
        //Arrays.asList(dish.getId()));

        List<DishFlavor> flavors = dishDTO.getFlavors();
        if (!CollectionUtils.isEmpty(flavors)) {    //新增口味时dish_id会为空，这里可以为口味的dish_id重新赋值，再添加到数据库
            flavors.forEach(flavor -> {
                flavor.setDishId(dishDTO.getId());
            });
            dishFlavorMapper.insertFlavorsOfDish(flavors);
        }
        //3、菜品缓存全删，查的时候再缓存
        cleanCache("*");
    }

    @Transactional
    @Override
    public void enableOrDisenable(Integer status, Long id) {
        Dish dish = Dish.builder().status(status).id(id).build();
        dishMapper.update(dish);
        if (status == StatusConstant.DISABLE) { //菜品停售 => 关联套餐也要停售
            //获取菜品关联套餐的所有id
            List<Long> setmealIdsByDishIds = setmealDishMapper.findSetmealIdsByDishIds(Collections.singletonList(id));
            if (!CollectionUtils.isEmpty(setmealIdsByDishIds)) {  //如果停售菜品有关联的套餐
                //停售每个关联套餐
                setmealIdsByDishIds.forEach(setmealId -> {   //获取每个要停售的套餐
                    Setmeal setmeal = Setmeal.builder().id(setmealId).status(StatusConstant.DISABLE).build();
                    setmealMapper.update(setmeal);
                });
            }
        }
        //菜品缓存全删，查的时候再缓存
        cleanCache("*");
    }

    @Override
    public List<Dish> listByInfo(DishPageQueryDTO dishPageQueryDTO) {
        dishPageQueryDTO.setStatus(StatusConstant.ENABLE);  //起售
        List<Dish> list = dishMapper.findByInfo(dishPageQueryDTO);
        return list;
    }


    /**
     * C端-根据分类id查询菜品(口味) 且菜品起售
     *
     * @param categoryId
     * @return
     */
    @Override
    public List<DishVO> findByCategoryId(Long categoryId) {
        //1、先查Redis缓存，如果缓存中有，则命中直接返回
        String redisDishKey = "dish:cache:" + categoryId;
        List<DishVO> dishVOList = (List<DishVO>) redisTemplate.opsForValue().get(redisDishKey);
        if (!CollectionUtils.isEmpty(dishVOList)) {
            log.info("查询Redis缓存，命中数据直接返回。");
            return dishVOList;
        }
        //2、如果缓存中没有，就去查数据库，
        DishDTO dishDTO = DishDTO.builder().categoryId(categoryId).status(StatusConstant.ENABLE).build();
        dishVOList = dishMapper.listDishWithFlavors(dishDTO);

        //3、查到的结果存入缓存
        redisTemplate.opsForValue().set(redisDishKey, dishVOList);
        log.info("查询数据库，把查到的数据缓存在Redis中。");
        return dishVOList;
    }

}
