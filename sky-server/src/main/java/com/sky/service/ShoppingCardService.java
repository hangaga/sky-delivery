package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCardService {
    /**
     * 添加购物车
     * @param shoppingCartDTO
     */
    void insert(ShoppingCartDTO shoppingCartDTO);

    /**
     * 查看当前用户的购物车数据
     * @return
     */
    List<ShoppingCart> list();

    /**
     * 删除当前用户购物车数据
     * @param shoppingCartDTO
     */
    void sub(ShoppingCartDTO shoppingCartDTO);

    /**
     * 清空当前用户购物车
     */
    void clear();
}
