package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface SetmealService {
    void insert(SetmealDTO setmealDTO);

    PageResult page(SetmealPageQueryDTO setmealPageQueryDTO);

    void deleteByIds(List<Long> ids);

    SetmealVO findById(Long id);

    void update(SetmealDTO setmealDTO);

    void enableOrDisenable(Integer status, Long id);

    /**
     * C端-根据分类id查询套餐(起售)
     * @param categoryId
     * @return
     */
    List<Setmeal> findEnableSetmealByCategoryId(Long categoryId);

    /**
     * C端-根据套餐ID查询包含的菜品列表
     * @param id
     * @return
     */
    List<DishItemVO> findDishBySetmealId(Long id);
}
