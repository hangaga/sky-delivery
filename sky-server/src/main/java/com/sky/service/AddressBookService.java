package com.sky.service;

import com.sky.entity.AddressBook;

import java.util.List;

public interface AddressBookService {
    /**
     * 查询当前用户的地址列表
     * @return
     */
    List<AddressBook> list();

    /**
     * 新增当前用户收货地址
     * @param addressBook
     */
    void insert(AddressBook addressBook);

    /**
     * 根据id查询地址
     * @param id
     * @return
     */
    AddressBook findById(Long id);

    /**
     * 根据当前用户ID修改地址信息
     * @param addressBook
     */
    void update(AddressBook addressBook);

    /**
     * 根据当前用户ID删除地址信息
     * @param id
     */
    void deleteById(Long id);

    /**
     * 设置默认的收货地址
     * @param addressBook
     */
    void updateDefault(AddressBook addressBook);

    /**
     * 查询当前登录用户默认的收货地址
     * @return
     */
    AddressBook getDefaultAddressBook();
}
