package com.sky.service;

import com.sky.vo.OrderReportVO;
import com.sky.vo.SalesTop10ReportVO;
import com.sky.vo.TurnoverReportVO;
import com.sky.vo.UserReportVO;

import java.io.IOException;
import java.time.LocalDate;

public interface ReportService {
    /*
        统计指定时间段中每一天的营业额
     */
    TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end);

    /**
     * 统计 指定时间内 用户新增数量及总数量
     * @param begin
     * @param end
     * @return
     */
    UserReportVO userStatistics(LocalDate begin, LocalDate end);

    /**
     *统计 指定时间内 订单的总量、有效订单量、有效订单率等数据
     * @param begin
     * @param end
     * @return
     */
    OrderReportVO ordersStatistics(LocalDate begin, LocalDate end);

    /**
     *统计 指定时间内 销量排名前10的菜品/套餐数据
     * @param begin
     * @param end
     * @return
     */
    SalesTop10ReportVO top10(LocalDate begin, LocalDate end);

    /**
     * 导出近30天的数据
     */
    void export() throws Exception;
}
