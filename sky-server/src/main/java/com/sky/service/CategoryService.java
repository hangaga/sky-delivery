package com.sky.service;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;

import java.util.List;

public interface CategoryService {
    PageResult list(CategoryPageQueryDTO categoryPageQueryDTO);

    Category findById(Long id);

    void update(CategoryDTO categoryDTO);

    void enableOrDisenable(Integer status, Long id);

    void insert(CategoryDTO categoryDTO);

    void deleteById(Long id);

    List<Category> findByType(Integer type);

    /**
     * C端-分类查询 且状态启用
     * @param type
     * @return
     */
    List<Category> findByTypeWithEnable(Integer type);
}
