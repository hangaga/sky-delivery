package com.sky.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@ServerEndpoint("/ws/{sid}")
public class WebSocketServer {

    private static Map<String, Session> sessionMap = new HashMap<>();

    @OnOpen//建立连接
    public void onOpen(Session session, @PathParam("sid") String sid) {
        log.info("建立连接:{}", sid);
        sessionMap.put(sid, session);
    }

    @OnMessage
    public void onMessage(String message, @PathParam("sid") String sid) {
        log.info("服务器收到来自客户端 " + sid + " 的消息:{}", message);
    }

    @OnError
    public void onError(Session session, @PathParam("sid") String sid, Throwable throwable) {
        log.info("出异常了");
    }

    @OnClose
    public void onClose(Session session, @PathParam("sid") String sid) {
        log.info("关闭连接:{}", sid);
        sessionMap.remove(sid);
    }


    public void sendMessageToAllClient(String message) throws Exception {
        Collection<Session> sessions = sessionMap.values();
        if (!CollectionUtils.isEmpty(sessions)) {
            for (Session session : sessions) {
                session.getBasicRemote().sendText(message);
            }

        }

    }

}
