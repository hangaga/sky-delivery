package com.sky.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class BaiduMapUtils {

    @Value("${sky.baidu.ak}")
    private String ak;
    @Value("${sky.baidu.geoCoderUrl}")
    private String geoCoderUrl;
    @Value("${sky.baidu.drivingUrl}")
    private String drivingUrl;

    //https://api.map.baidu.com/geocoding/v3/?address=北京市海淀区上地十街10号&output=json&ak=您的ak&callback=showLocation

    /**
     * 解析纬度（保留6位小数）
     *
     * @param address
     * @return
     */
    public String getLat(String address) {
        String jsonResult = geocodingJsonResult(address);
        String lat = JSONObject.parseObject(jsonResult)
                .getJSONObject("result")
                .getJSONObject("location")
                .getBigDecimal("lat")
                .setScale(6, RoundingMode.HALF_UP)
                .toString();
        return lat;
    }


    /**
     * 解析经度（保留6位小数）
     *
     * @param address
     * @return
     */
    public String getLng(String address) {
        String jsonResult = geocodingJsonResult(address);
        String lng = JSONObject.parseObject(jsonResult)
                .getJSONObject("result")
                .getJSONObject("location")
                .getBigDecimal("lng")
                .setScale(6, RoundingMode.HALF_UP)
                .toString();
        return lng;
    }

    /**
     * GET请求，获取地理编码的Json的字符串类型
     *
     * @param address
     * @return
     */
    private String geocodingJsonResult(String address) {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("address", address);
        paramMap.put("output", "json");
        paramMap.put("ak", ak);
        return HttpClientUtil.doGet(geoCoderUrl, paramMap);
    }


    //https://api.map.baidu.com/directionlite/v1/driving?origin=40.01116,116.339303&destination=39.936404,116.452562&ak=您的AK

    /**
     * GET请求，获取驾车路线规划,返回两地距离
     * origin 格式为：纬度,经度
     * destination 格式为：纬度,经度
     * @param originLat      起始地纬度
     * @param originLng      起始地经度
     * @param destinationLat 终点纬度
     * @param destinationLng 终点经度
     * @return
     */
    public Long getDistance(String originLat, String originLng, String destinationLat, String destinationLng) {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("origin", originLat + "," + originLng);
        paramMap.put("destination", destinationLat + "," + destinationLng);
        paramMap.put("ak",ak);
        String jsonResult = HttpClientUtil.doGet(drivingUrl, paramMap);
        String route = JSONObject.parseObject(jsonResult).getJSONObject("result").getJSONArray("routes").get(0).toString();
        Long distance = JSONObject.parseObject(route).getLong("distance");
        return distance;
    }


}
