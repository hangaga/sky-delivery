package com.sky.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "sky.baidu")
public class BaiduMapProperties {
    /**
     * 百度地图相关配置
     */
    private String ak;
    private String geoCoderUrl;
    private String drivingUrl;
}