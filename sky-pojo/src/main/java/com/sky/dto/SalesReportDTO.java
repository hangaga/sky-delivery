package com.sky.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SalesReportDTO implements Serializable {
    //商品名称
    private String goodsName;

    //销量
    private Integer goodsNumber;
}
